---
title: CGRL Accounts
keywords: 
last_updated: October 1, 2019
tags: [hpc, cgrl, accounts]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/getting-account/cgrl-account
folder: hpc
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: The Computational Genomics Resource Laboratory  CGRL  provides access to two computing clusters collocated within the larger Savio system administered by Berkeley Research Computing at the University of California  Berkeley  Vector is a heterogeneous cluster that is accessed through the Savio login nodes  but it is independent from the rest of Savio and exclusively used by the CGRL  Rosalind is a condo within Savio  Through the  condo model of access  site baseurl  services high performance computing condos  CGRL users can utilize a number of Savio nodes equal to those contributed by Rosalind    Account requests You can request new accounts through the CGRL by emailing cgrl berkeley edu  
---

The <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">Computational Genomics Resource Laboratory (CGRL)</a> provides access to two computing clusters collocated within the larger Savio system administered by <a href="http://research-it.berkeley.edu/programs/berkeley-research-computing" target="_blank">Berkeley Research Computing</a> at the University of California, Berkeley. Vector is a heterogeneous cluster that is accessed through the Savio login nodes, but it is independent from the rest of Savio and exclusively used by the CGRL. Rosalind is a condo within Savio. Through the [condo model of access]({{ site.baseurl }}/services/high-performance-computing/condos/), CGRL users can utilize a number of Savio nodes equal to those contributed by Rosalind.

#### Account requests

You can request new accounts through the <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">CGRL</a> by emailing <a href="mailto:cgrl@berkeley.edu">cgrl@berkeley.edu</a>.
